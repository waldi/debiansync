# -*- coding: utf-8 -*-
# Copyright: 2017 Bastian Blank
# License: MIT, see LICENSE for details.


import collections
import collections.abc
import logging
import socket
import urllib.parse
import yaml


logger = logging.getLogger(__name__)


class Config(collections.abc.Mapping):
    schema = {
        'sync': {
            'mirrorname': (str, socket.gethostname()),
            'source': (urllib.parse.urlparse, False),
            'target': (urllib.parse.urlparse, False),
            'rsync_auth': (str, None),
            'rsync_filter': (list, []),
        },
        'push': {
        },
    }

    def __init__(self):
        self._archives = {}

    def __getitem__(self, k):
        return self._archives[k]

    def __iter__(self):
        return iter(self._archives)

    def __len__(self):
        return len(self._archives)

    def read_yaml(self, f):
        for archive, tools in yaml.safe_load(f).items():
            if archive.startswith('.'):
                continue

            for tool, info in tools.items():
                if not tool in self.schema:
                    logging.debug('{}:{}: unknown tool "{}", ignoring'.format(f.name, archive, tool))
                    continue

                config = {}
                for key, (factory, default) in self.schema[tool].items():
                    try:
                        config[key] = factory(info[key])
                    except KeyError:
                        if default is False:
                            raise RuntimeError
                        config[key] = default

                self._archives.setdefault(archive, {})[tool] = config

    def get_archives(self, tools, archives=None):
        ret = collections.OrderedDict()

        for archive in archives or sorted(self.keys()):
            i = self[archive]
            o = []
            c = 0

            for tool in tools:
                if tool in i:
                    o.append(i[tool])
                    c += 1
                else:
                    o.append(None)

            if c:
                ret[archive] = o
            else:
                raise RuntimeError('No usable config for archive {}'.format(archive))

        return ret
