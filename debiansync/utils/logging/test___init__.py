# -*- coding: utf-8 -*-
# Copyright: 2017 Bastian Blank
# License: MIT, see LICENSE for details.


import logging
import pytest

from . import PreRotatingFileHandlerContext


def test_PreRotatingFileHandlerContext(tmpdir):
    logdir = tmpdir.mkdir('log')
    log = logdir.join('logfile').strpath

    with PreRotatingFileHandlerContext(log, backupCount=2):
        assert sorted(i.basename for i in logdir.listdir()) == ['logfile']
        logging.error('log')

    with PreRotatingFileHandlerContext(log, backupCount=2):
        assert sorted(i.basename for i in logdir.listdir()) == ['logfile', 'logfile.1']
        logging.error('log')

    with PreRotatingFileHandlerContext(log, backupCount=2):
        assert sorted(i.basename for i in logdir.listdir()) == ['logfile', 'logfile.1', 'logfile.2']
        logging.error('log')

    with PreRotatingFileHandlerContext(log, backupCount=2):
        assert sorted(i.basename for i in logdir.listdir()) == ['logfile', 'logfile.1', 'logfile.2']
        logging.error('log')
