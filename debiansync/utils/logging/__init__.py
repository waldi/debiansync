# -*- coding: utf-8 -*-
# Copyright: 2017 Bastian Blank
# License: MIT, see LICENSE for details.


from __future__ import absolute_import

import contextlib
import logging
import logging.handlers
import socket


class PreRotatingFileHandler(logging.handlers.RotatingFileHandler):
    def __init__(self, filename, mode='x', maxBytes=0, backupCount=0, encoding=None):
        super().__init__(filename, mode, maxBytes, backupCount, encoding, delay=True)
        # Hack to find out file can be written to early
        self.delay = False
        self.doRollover()


@contextlib.contextmanager
def PreRotatingFileHandlerContext(filename, backupCount=9):
    logger = logging.getLogger()
    handler = PreRotatingFileHandler(
            filename, backupCount=backupCount, encoding='utf-8')
    handler.setFormatter(logging.Formatter(
        '%(asctime)s {} %(message)s'.format(socket.gethostname())))
    handler.setLevel(logging.INFO)
    logger.addHandler(handler)
    yield handler
    logger.removeHandler(handler)
