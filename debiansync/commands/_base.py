# -*- coding: utf-8 -*-
# Copyright: 2017 Bastian Blank
# License: MIT, see LICENSE for details.

import appdirs
import argparse
import collections
import logging
import os
import sys

from ..config import Config


logger = logging.getLogger(__name__)


def setup_argparse(description, usage='%(prog)s [OPTION]... [ARCHIVE]...'):
    parser = argparse.ArgumentParser(description=description,
            add_help=False,
            usage=usage)
    parser.add_argument('archives', help=argparse.SUPPRESS, nargs='*')
    parser_group = parser.add_argument_group(title='Global arguments')
    parser_group.add_argument('--config', metavar='CONFIG', default='debiansync.yaml')
    parser_group.add_argument('--debug', action='store_true')
    parser_group.add_argument('--help', action='help')
    parser_group.add_argument('--log', metavar='DIRECTORY', default='log')
    return parser


class CommandBase:
    parser = setup_argparse(description='Base')

    def __init__(self, args=None):
        self.args = self.parser.parse_args(args)

        logging.basicConfig(level=self.args.debug and logging.DEBUG or logging.INFO)

        self.config = Config()

        for configdir in ('.', appdirs.user_config_dir()):
            configfile = os.path.join(configdir, self.args.config)
            if os.path.exists(configfile):
                logging.debug('Read config file %s', configfile)
                with open(configfile) as c:
                    self.config.read_yaml(c)
                break
        else:
            logging.error('No config found')
            sys.exit(1)
