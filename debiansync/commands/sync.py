# -*- coding: utf-8 -*-
# Copyright: 2017 Bastian Blank
# License: MIT, see LICENSE for details.

import os.path

from ..sync import Sync
from ..utils.logging import PreRotatingFileHandlerContext
from ._base import CommandBase, setup_argparse


class Command(CommandBase):
    parser = setup_argparse(description='Sync')

    def __call__(self):
        for archive, (config, ) in self.config.get_archives(('sync', )).items():
            Sync(config, self.args.log, archive).do_all()


if __name__ == '__main__':
    Command()()
