# -*- coding: utf-8 -*-
# Copyright: 2017 Bastian Blank
# License: MIT, see LICENSE for details.

import logging
import subprocess
import sys


logger = logging.getLogger(__name__)


class RsyncBase:
    def __init__(self, config):
        self.config = config

    def rsync(self, out, args):
        a = [
            'rsync',
            '-prltvHSB8192',
            '--timeout=3600',
            '--stats',
            '{}::{}'.format(self.config['source'].netloc, self.config['source'].path.lstrip('/')),
            self.config['target'].path,
        ]
        a.extend(self.config.get('rsync_filter', []))

        try:
            logger.info('Running: {}'.format(' '.join(a)))
            subprocess.run(a, check=True, stdout=out, stderr=subprocess.PIPE)

        except subprocess.CalledProcessError as e:
            logger.error('rsync failed to run')
            for i in e.stderr.splitlines():
                logger.error(i.decode('ascii', 'replace'))
            sys.exit(1)
