# -*- coding: utf-8 -*-
# Copyright: 2017 Bastian Blank
# License: MIT, see LICENSE for details.

from . import RsyncBase


class RsyncStage2(RsyncBase):
    def __call__(self, out):
        self.rsync(out, [
            '--max-delete=40000',
            '--delay-updates',
            '--delete',
            '--delete-after',
            '--delete-excluded',
        ])
