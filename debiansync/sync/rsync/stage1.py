# -*- coding: utf-8 -*-
# Copyright: 2017 Bastian Blank
# License: MIT, see LICENSE for details.

from . import RsyncBase


class RsyncStage1(RsyncBase):
    def __call__(self, out):
        self.rsync(out, [
            '--exclude=Packages*',
            '--exclude=Sources*',
            '--exclude=Release*',
            '--exclude=InRelease',
            '--include=i18n/by-hash/**',
            '--exclude=i18n/*',
            '--exclude=ls-lR*',
        ])
