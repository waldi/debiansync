# -*- coding: utf-8 -*-
# Copyright: 2017 Bastian Blank
# License: MIT, see LICENSE for details.


import contextlib
import os

from ..utils.logging import PreRotatingFileHandler, PreRotatingFileHandlerContext
from .flag import Flag
from .lock import Lock
from .rsync.stage1 import RsyncStage1
from .rsync.stage2 import RsyncStage2


class Sync:
    def __init__(self, config, logdir, logname):
        self.config = config
        self.logdir = logdir
        self.logname = logname

        self.mirrorname = self.config['mirrorname']
        self.path = self.config['target'].path

        self.flag = os.path.join(self.path, 'Archive-Update-Required-{}'.format(self.mirrorname))
        self.lock = os.path.join(self.path, 'Archive-Update-in-Progress-{}'.format(self.mirrorname))

    def do_all(self):
        flag = Flag(self.flag)

        with self.context() as rsyncout:
            while flag.check():
                RsyncStage1(self.config)(rsyncout)
                while flag.check():
                    RsyncStage1(self.config)(rsyncout)

                RsyncStage2(self.config)(rsyncout)

    @contextlib.contextmanager
    def context(self):
        logfile = os.path.join(self.logdir, 'sync_{}.log'.format(self.logname))
        rsyncoutfile = os.path.join(self.logdir, 'sync_{}.rsyncout'.format(self.logname))

        with Lock(self.lock):
            with PreRotatingFileHandlerContext(logfile, backupCount=9):
                rsyncout = PreRotatingFileHandler(rsyncoutfile, backupCount=9, encoding='utf-8')
                yield rsyncout.stream
