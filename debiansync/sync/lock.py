# -*- coding: utf-8 -*-
# Copyright: 2017 Bastian Blank
# License: MIT, see LICENSE for details.


import logging
import os


logger = logging.getLogger(__name__)


class Lock:
    def __init__(self, lock, *, _pid=None):
        self.lock = lock
        if _pid is None:
            self._pid = os.getpid()
        else:
            self._pid = _pid
        self._locked = False

    def __enter__(self):
        if self._locked:
            raise RuntimeError

        try:
            self._lock()
            return

        except FileExistsError:
            pass

        self._check()
        self._lock()

    def __exit__(self, type, value, traceback):
        if not self._locked:
            raise RuntimeError

        self._unlock()

    def _check(self):
        with open(self.lock, 'r') as f:
            pid = int(f.readline().strip())
            try:
                os.kill(pid, 0)
            except OSError:
                pass
            else:
                # Process does either not exist or is not owned by us.
                self._unlock()

    def _lock(self):
        with open(self.lock, 'x') as f:
            f.write('{}\n'.format(self._pid))
            f.flush()

        logger.debug('Locked %s (pid %s)', self.lock, self._pid)
        self._locked = True

    def _unlock(self):
        try:
            os.unlink(self.lock)
        except FileNotFoundError:
            pass

        logger.debug('Unlocked %s (pid %s)', self.lock, self._pid)
        self._locked = False
