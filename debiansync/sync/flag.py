# -*- coding: utf-8 -*-
# Copyright: 2017 Bastian Blank
# License: MIT, see LICENSE for details.


import logging
import os


logger = logging.getLogger(__name__)


class Flag:
    def __init__(self, flag):
        self.flag = flag
        self.arm()

    def _remove(self):
        try:
            os.unlink(self.flag)
        except FileNotFoundError:
            pass

    def arm(self):
        logger.debug('Arming flag file {}'.format(self.flag))
        open(self.flag, 'wb')

    def check(self):
        if os.path.exists(self.flag):
            logger.info('Checking flag file {}, still existing'.format(self.flag))
            self._remove()
            return True
        else:
            logger.debug('Checking flag file {}, not existing'.format(self.flag))
            return False
