# -*- coding: utf-8 -*-
# Copyright: 2017 Bastian Blank
# License: MIT, see LICENSE for details.


import os
import pytest

from .lock import Lock


def test_lock(tmpdir):
    lock = Lock(tmpdir.dirpath('lock').strpath)

    with lock:
        pass
    with lock:
        pass

def test_lock_reent(tmpdir):
    lock = Lock(tmpdir.dirpath('lock').strpath, _pid=1)
    lock1 = Lock(tmpdir.dirpath('lock').strpath)

    with lock:
        with pytest.raises(FileExistsError):
            with lock1:
                pass

def test_lock_override(tmpdir):
    lock = Lock(tmpdir.dirpath('lock').strpath)
    lock1 = Lock(tmpdir.dirpath('lock').strpath)

    with lock:
        with lock1:
            pass

def test_unlock(tmpdir):
    lock = Lock(tmpdir.dirpath('lock').strpath)
    lock._unlock()
