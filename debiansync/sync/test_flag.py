# -*- coding: utf-8 -*-
# Copyright: 2017 Bastian Blank
# License: MIT, see LICENSE for details.


import os
import pytest

from .flag import Flag


def test_flag(tmpdir):
    flag = Flag(tmpdir.dirpath('flag').strpath)

    assert flag.check() is True

    flag.arm()
    assert flag.check() is True

    assert flag.check() is False


def test_flag_remove(tmpdir):
    flag = Flag(tmpdir.dirpath('flag').strpath)

    flag._remove()
    flag._remove()
